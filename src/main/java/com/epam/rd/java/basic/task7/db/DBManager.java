package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static Connection connection;

	public static synchronized DBManager getInstance() {
		try {
			if (instance == null) {
				instance = new DBManager();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return  instance;
	}

	private DBManager() throws SQLException {
		connection = DriverManager.getConnection(getConnection());
	}

	private String getConnection() {
		Properties properties = new Properties();
		try (InputStream stream = Files.newInputStream(Paths.get("app.properties"))){
			properties.load(stream);
		}catch (IOException e){
			e.printStackTrace();
		}
		return properties.getProperty("connection.url");
	}

	public List<User> findAllUsers() throws DBException {
		List<User> people = new ArrayList<>();
		String sql = "SELECT * FROM Users";

		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				User user = new User();

				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));

				people.add(user);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return people;
	}

	public boolean insertUser(User user) throws DBException {
		String sql = "INSERT INTO Users(login) VALUES(?)";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
		{
			preparedStatement.setString(1, user.getLogin());
			preparedStatement.execute();

			user.setId(getUser(user.getLogin()).getId());

			return true;
		} catch (SQLException throwables){
			throwables.printStackTrace();
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		String sql = "DELETE FROM users WHERE id =?";
		try (PreparedStatement getUserQuery = connection.prepareStatement(sql)){
			for (User user : users) {
				getUserQuery.setInt(1, user.getId());
				getUserQuery.executeUpdate();
			}
			return  true;
		} catch (SQLException throwables){
			throwables.printStackTrace();
		}

		return false;
	}

	public User getUser(String login) throws DBException {
		String sql = "SELECT * FROM Users WHERE login=?";
		User user = null;

		try (PreparedStatement getUserQuery = connection.prepareStatement(sql)) {
			getUserQuery.setString(1, login);

			ResultSet resultSet = getUserQuery.executeQuery();
			resultSet.next();

			user = new User();
			user.setId(resultSet.getInt("id"));
			user.setLogin(resultSet.getString("login"));
		}catch (SQLException throwables) {
			throwables.printStackTrace();
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		String sql = "SELECT * FROM Teams WHERE name=?";
		Team team = null;

		try (PreparedStatement getUserQuery = connection.prepareStatement(sql)){
			getUserQuery.setString(1, name);

			ResultSet resultSet = getUserQuery.executeQuery();
			resultSet.next();

			team= new Team();
			team.setId(resultSet.getInt("id"));
			team.setName(resultSet.getString("name"));
		} catch (SQLException throwables){
			throwables.printStackTrace();
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		String sql = "SELECT * FROM Teams";
		try (Statement statement = connection.createStatement()){
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				Team team = new Team();

				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));

				teams.add(team);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}

		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		String sql = "INSERT INTO teams(name) VALUES(?)";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

			preparedStatement.setString(1, team.getName());
			preparedStatement.execute();

			team.setId(getTeam(team.getName()).getId());
			return true;
		} catch (SQLException throwables) {

				throwables.printStackTrace();

		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String sql = "INSERT INTO users_teams(user_id, team_id) VALUES(?, ?)";

		try (PreparedStatement updateTeamsForUser = connection.prepareStatement(sql)){
			connection.setAutoCommit(false);

			for(Team team : teams) {
				updateTeamsForUser.setInt(1, user.getId());
				updateTeamsForUser.setInt(2, team.getId());
				updateTeamsForUser.executeUpdate();
			}
			connection.commit();

			connection.setAutoCommit(true);
			return true;
		}catch (SQLException throwables) {
			try {
				connection.rollback();
			}catch (SQLException e){
				e.printStackTrace();
			}
			throw new DBException("transaction has  been failed", new SQLException());
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		String sql = "SELECT teams.id, teams.name FROM teams LEFT JOIN users_teams ut on teams.id = ut.team_id WHERE ut.user_id = ?";

		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setInt(1, user.getId());
			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				teams.add(team);
			}
		} catch (SQLException throwables){
			throwables.printStackTrace();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String sql = "DELETE FROM teams WHERE name =?";

		try (PreparedStatement getUserQuery = connection.prepareStatement(sql)){
			getUserQuery.setString(1, team.getName());
			getUserQuery.executeUpdate();
			return true;
		}catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		String sql = "UPDATE Teams SET name = ? WHERE id =?";

		try (PreparedStatement updateTeam = connection.prepareStatement(sql)){
			updateTeam.setString(1, team.getName());
			updateTeam.setInt(2, team.getId());

			updateTeam.executeUpdate();
			return true;
		}catch (SQLException throwables) {
			throwables.printStackTrace();
		}

		return false;
	}

}
